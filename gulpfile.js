var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglifyjs');
var rename = require('gulp-rename');
var minifyHtml = require('gulp-minify-html');
var htmlbuild = require('gulp-htmlbuild');
var csso = require('gulp-csso');

var CSSfilesToConcat = [
    'src/css/bootstrap.min.css',
    'src/css/lightslider.css',
    'src/css/style.css'
];

var JSfilesToConcat = [
    'src/scripts/jquery.min.js',
    'src/scripts/lodash.min.js',
    'src/scripts/braintree-2.21.0.min.js',
    'src/scripts/bootstrap.min.js',
    'src/scripts/lock-9.0.js',
    'src/scripts/jquery-ui.js',
    'src/scripts/lightslider.js',
    'src/scripts/countries.js'
];

var date = new Date();
version = date.getFullYear()+"-"+date.getMonth()+"-"+date.getDate()+"_"+date.getHours()+"-"+date.getMinutes()+"-"+date.getSeconds();

gulp.task('deleteFontsFolder', require('del').bind(null, ['dist/fonts']));

gulp.task('deleteCSSFolder', require('del').bind(null, ['dist/css']));

gulp.task('deleteJSFolder', require('del').bind(null, ['dist/js']));

gulp.task('deleteScriptsFolder', require('del').bind(null, ['dist/scripts']));

gulp.task('deleteImagesFolder', require('del').bind(null, ['dist/images']));

gulp.task('deleteImgFolder', require('del').bind(null, ['dist/img']));

gulp.task('deleteHTMLFolder', require('del').bind(null, ['dist/views']));



gulp.task('js', ['deleteScriptsFolder'], function() {
    return gulp.src('src/scripts/**/*.js')
        .pipe(gulp.dest('dist/scripts'));
});

gulp.task('scripts', ['deleteJSFolder'], function() {
    gulp.src('src/js/main.js')
        .pipe(browserify({ transform: 'reactify' }))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename('main.min.js'))
        .pipe(uglify('main.min.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('fonts', ['deleteFontsFolder'], function() {
    return gulp.src('src/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('images', ['deleteImagesFolder'], function() {
    return gulp.src('src/images/**/*')
        .pipe(cache(imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/images'));
});

gulp.task('img', ['deleteImgFolder'], function() {
    return gulp.src('src/img/**/*')
        .pipe(cache(imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('styles', ['deleteCSSFolder'], function() {
	return gulp.src('src/css/**/*.css').pipe(gulp.dest('dist/css'));
});

/*gulp.task('styles', ['deleteCSSFolder'],function() {
    var lazypipe = require('lazypipe');
    var cssChannel = lazypipe()
        .pipe(csso);
    return gulp.src(CSSfilesToConcat)
        .pipe(concat('style.css'))
        .pipe(cssChannel())
        .pipe(gulp.dest('dist/css'));
});*/

gulp.task('html', ['deleteHTMLFolder'], function() {
    return gulp.src('src/views/**/*.html')
        .pipe(minifyHtml({ conditionals: true, loose: true, empty: true }))
        .pipe(gulp.dest('dist/views'));
});

gulp.task('vendors', ['deleteScriptsFolder'], function() {
    return gulp.src(JSfilesToConcat)
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest('dist/scripts'));
});

gulp.task('build',['deleteHTMLFolder','deleteCSSFolder','deleteJSFolder'], function() {
    gulp.src('src/views/**/*.html')
        .pipe(htmlbuild({
            js: htmlbuild.preprocess.js(function(block) {
                block.write('js/main_' + version +'.js');
                block.end();
            }),
            css: htmlbuild.preprocess.css(function (block) {
                block.write('css/style_' + version +'.css');
                block.end();
              }),
        }))
        .pipe(minifyHtml({ conditionals: true, loose: true, empty: true }))
        .pipe(gulp.dest('dist/views'));
    gulp.src('src/js/main.js')
        .pipe(browserify({ transform: 'reactify' }))
        .pipe(concat('main_'+version+'.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename('main.min_'+version+'.js'))
        .pipe(uglify('main.min_'+version+'.js'))
        .pipe(gulp.dest('dist/js'));
    var lazypipe = require('lazypipe');
    var cssChannel = lazypipe()
        .pipe(csso);
    return gulp.src(CSSfilesToConcat)
        .pipe(concat('style_'+version+'.css'))
        .pipe(cssChannel())
        .pipe(gulp.dest('dist/css'));
});


gulp.task('connect', function() {
    connect.server({
        root: ['dist'],
        port: '9000',
        base: 'http://localhost'
    });
});

gulp.task('default', ['js', 'images', 'img', 'scripts', 'vendors', 'fonts', 'styles', 'html']);

gulp.task('start', ['scripts', 'connect', ]);

gulp.task('watch', function() {
    gulp.watch('src/**/*.*', ['default']);
});
