/** @jsx React.DOM */
'use strict';
var React = require('react');

var Default = React.createClass({
  render:function () {
    return (
      <div>
        <h1>Default Page</h1>
        <a href="#/home">Home</a><br/>
      </div>
    );  
  }
});

module.exports = Default;