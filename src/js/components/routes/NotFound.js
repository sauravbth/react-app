/** @jsx React.DOM */
'use strict';
var React = require('react');

var NotFound = React.createClass({
  render:function () {
    return (
      <div>
        <h1>404 Page</h1>
        <a href="#/home">Home</a><br/>
      </div>
    );  
  }
});

module.exports = NotFound;