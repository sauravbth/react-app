/** @jsx React.DOM */
'use strict';
var React = require('react');
var Router = require('react-router');
var Navigation = Router.Navigation;

var AppConstants = require('../../constants/AppConstants');
var AppUtils = require('../../utils/AppUtils');
var AppActions = require('../../actions/AppActions');
var AppFunctions = require('../../utils/AppFunctions');
var AppStore = require('../../stores/AppStore');

var HomeBody = require('../HomeBody');

var Home = React.createClass({
    mixins: [Navigation],
    getInitialState: function() {
        return null;     
    },
    componentDidMount: function() {    
    },
    componentWillUnmount: function() {
    },
    render: function() {
        return ( 
            <div>
                <h1>HOME</h1>
                <HomeBody /> 
            </div>
        );
    },
    _onChange: function() {
    }
});
module.exports = Home;