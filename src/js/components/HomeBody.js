/** @jsx React.DOM */
'use strict';
var React = require('react');

var AppActions = require('../actions/AppActions');

var HomeBody = React.createClass({
    handleClick1:function () {
        console.log("click1");
        AppActions.handleAlert1("click 1");
    },
    handleClick2:function () {
        console.log("click2");
        AppActions.handleAlert2("click 1");
    },
    render:function () {
        return (
          	<div>
    	          HOME PAGE
                <br />
                <a onClick={this.handleClick1}>Click 1</a>
                <br />
                <a onClick={this.handleClick2}>Click 2</a>
          	</div>
        );  
    }
});

module.exports = HomeBody;