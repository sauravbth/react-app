/** @jsx React.DOM */
'use strict';
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;
var NotFoundRoute = Router.NotFoundRoute;

var AppActions = require('../actions/AppActions');
var AppAPI = require('../utils/AppAPI');

var Default = require('./routes/Default');
var Home = require('./routes/Home');
var NotFound = require('./routes/NotFound');


/** routing */
var routes = (
  	<Route>
		<DefaultRoute handler={Home} />
		<Route name="home" handler={Home} />
		<NotFoundRoute handler={NotFound}/>
   	</Route>
);
module.exports = routes;