var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var AppConstants = require('../constants/AppConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppUtils = require('../utils/AppUtils');
var AppAPI = require('../utils/AppAPI');
var AppFunctions = require('../utils/AppFunctions');
var CHANGE_EVENT = 'change';

var AppStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppStore.dispatchToken = AppDispatcher.register(function(payload) {
// AppDispatcher.register(function(payload) {
    var action = payload;
    switch (action.actionType) {
        case AppConstants.HANDLE_ALERT1:
            console.log(1);
            AppStore.emitChange();
            break;
        case AppConstants.HANDLE_ALERT2:
            console.log(2);
            AppStore.emitChange();
            break;
        default:
            return true;
    }
    return true;
});
module.exports = AppStore;
