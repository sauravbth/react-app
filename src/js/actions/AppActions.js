var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var AppActions = {
    handleAlert1 : function(item) {
        AppDispatcher.dispatch({
            actionType: AppConstants.HANDLE_ALERT1,
            item: item
        })
    },
    handleAlert2 : function(item) {
        AppDispatcher.dispatch({
            actionType: AppConstants.HANDLE_ALERT2,
            item: item
        });
    },
}

module.exports = AppActions;
