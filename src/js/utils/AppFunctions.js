var AppActions = require('../actions/AppActions');
var AppUtils = require('../utils/AppUtils');
var AppConstants = require('../constants/AppConstants');

module.exports = {
    md5: function(value) {
        return CryptoJS.MD5(value).toString();
    },
    saveToLocalStorage: function(data) {
        _.each(data, function(keyValue, keyName) {
            localStorage.setItem(keyName, keyValue);
        });
    },
    removeFromLocalStorage: function(data) {
        _.each(data, function(item) {
            localStorage.removeItem(item);
        });
    },
    clearLocalStorage: function(){
        localStorage.clear();
    },
    capitalizeWords: function(str) {
        return str.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });  
    }
};
