var AppFunctions = require('../utils/AppFunctions');
var AppActions = require('../actions/AppActions');
var AppUtils = require('../utils/AppUtils');
var AppConstants = require('../constants/AppConstants');

function logout(api, data) {
    AppUtils.handleApiCall(api, "POST", data, function(error, result) {
        if (error) {
            console.log(error);
        } else {
            window.location.href = URL;
            location.reload();
            AppActions.handleLogout('logout');
        }
    });
}

module.exports = {
    ajaxCall: function(api, data) {
        switch (api) {
            case 'logOut':
                logout(api, data);
                break;                       
            default:
                return true;
        }
    }
};
