var AppConstants = require('../constants/AppConstants');
var AppActions = require('../actions/AppActions');

module.exports = {
    setCookie: function(cname, cvalue, duration) {
        var d = new Date();
        d.setTime(d.getTime() + (duration));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    },
    removeCookie: function(cname) {
        var d = new Date();
        d.setTime(d.getTime() + (-1));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + '' + "; " + expires;
    },
    checkTime: function(i) {
        if (i<10) {
            i = "0" + i
        };        
        return i;
    }
};
