/** @jsx React.DOM */
'use strict';
var React = require('react');
window.React = React;
var Router = require('react-router');

var App = require('./components/App');
var AppUtils = require('./utils/AppUtils');

Router.run(App, function (Handler) {
    React.render(<Handler/>, document.body);
});
